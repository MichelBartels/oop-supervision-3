import java.io.*;
import java.util.LinkedList;
import java.util.List;

public class CompositePattern {
    public static void main(String[] args) throws IOException {
        CompositeLogger logger = new CompositeLogger();
        logger.addLogger(new CLLogger());
        logger.addLogger(new FileLogger("logs.txt"));
        logger.log("Method called");
        logger.log("Method called again");
    }
}

interface Logger {
    void log(String message);
}
class CLLogger implements Logger {
    @Override
    public void log(String message) {
        System.out.println(message);
    }
}

class FileLogger implements Logger {
    String file;
    public FileLogger(String file) {
        this.file = file;
    }

    @Override
    public void log(String message) {
        try {
            Writer writer = new BufferedWriter(new FileWriter(file, true));
            writer.write(message + "\n");
            writer.close();
        } catch (IOException exception) {
            System.out.println("Can't log to " + file);
        }

    }
}
class CompositeLogger implements Logger {
    List<Logger> loggers;

    public CompositeLogger() {
        loggers = new LinkedList<>();
    }
    public void addLogger(Logger logger) {
        loggers.add(logger);
    }
    @Override
    public void log(String message) {
        for (Logger logger: loggers) {
            logger.log(message);
        }
    }
}