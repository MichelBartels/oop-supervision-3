public class SingletonCounter {
    private static SingletonCounter instance = new SingletonCounter();
    private int count = 0;
    private SingletonCounter() {}
    public static SingletonCounter getInstance() {
        return instance;
    }
    public int getCount() {
        return count;
    }
    public void count() {
        count++;
    }

    public static void main(String[] args) {
        SingletonCounter counter = SingletonCounter.getInstance();
        System.out.println(counter.getCount());
        counter.count();
        System.out.println(counter.getCount());
        SingletonCounter newCounter = SingletonCounter.getInstance();
        System.out.println(newCounter.getCount());
    }
}