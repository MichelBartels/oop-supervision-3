import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class Car {
    private String manufacturer;
    private int age;
    public Car(String manufacturer, int age) {
        this.manufacturer = manufacturer;
        this.age = age;
    }

    public String getManufacturer() {
        return manufacturer;
    }
    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "Car{" +
                "manufacturer='" + manufacturer + '\'' +
                ", age=" + age +
                '}';
    }

    public static void main(String[] args) {
        List cars = new LinkedList<>();
        cars.add(new Car("BMW", 10));
        cars.add(new Car("VW", 0));
        cars.add(new Car("BMW", 2));
        System.out.println(cars);
        cars.sort(new CarComparator());
        System.out.println(cars);
    }
}

class CarComparator implements Comparator<Car> {
    @Override
    public int compare(Car car1, Car car2) {
        int manufacturerOrder = car1.getManufacturer().compareTo(car2.getManufacturer());
        if (manufacturerOrder != 0) {
            return manufacturerOrder;
        }
        return car1.getAge() - car2.getAge();
    }
}