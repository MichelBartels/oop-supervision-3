public class Exercise1 {
    public static void main(String[] args) {
        ClassWithStaticMethodAndField.publicStaticField = "Test2";
        System.out.println(ClassWithStaticMethodAndField.publicStaticField);
        System.out.println(ClassWithStaticMethodAndField.staticMethod());
    }
}

class ClassWithStaticMethodAndField {
    public static String publicStaticField = "Test";
    private static String privateStaticField = "Test";
    public static String staticMethod() {
        return privateStaticField;
    }
}